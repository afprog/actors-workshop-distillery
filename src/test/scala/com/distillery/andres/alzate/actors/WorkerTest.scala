package com.distillery.andres.alzate.actors

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import com.distillery.andres.alzate.exceptions.PseudoRandomStringGenerationException
import com.distillery.andres.alzate.moldel.{ProcessCompleted, Report, Start, WorkerStatus}
import com.distillery.andres.alzate.service.PseudoRandomDataProvider
import com.distillery.andres.alzate.moldel.Report
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

import scala.concurrent.duration.DurationDouble
import scala.language.postfixOps

class WorkerTest extends TestKit(ActorSystem("worker-test-system")) with WordSpecLike with BeforeAndAfterAll {

  "Worker" should {
    "send completed report to supervisor " in {
      val supervisor = TestProbe()
      val provider = new PseudoRandomDataProvider {
        override def getData(): String = "Lpfn"
      }

      val worker = system.actorOf(Worker.props(provider, 3 seconds, supervisor.ref))
      worker ! Start

      supervisor.expectMsgType[ProcessCompleted](5 seconds)
    }

    "send failure stats to parent" in {
      val supervisor = TestProbe()
      val provider = new PseudoRandomDataProvider {
        override def getData(): String =
          throw PseudoRandomStringGenerationException(message = "PseudoRandom Generator Fail")
      }

      val worker = system.actorOf(Worker.props(provider, 3 seconds, supervisor.ref))
      worker ! Start

      supervisor.expectMsg(5 seconds, ProcessCompleted(Report(WorkerStatus.FAILURE, None, None)))
    }

    "send timeout stats to supervisor" in {
      val supervisor = TestProbe()

      val provider = new PseudoRandomDataProvider {
        override def getData(): String = "test"
      }

      val worker = system.actorOf(Worker.props(provider, 3 seconds, supervisor.ref))

      worker ! Start
      supervisor.expectMsg(5 seconds, ProcessCompleted(Report(WorkerStatus.TIMEOUT, None, None)))
    }
  }

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }
}
