package com.distillery.andres.alzate.service

trait DataProvider {
  def getData(): String
}
