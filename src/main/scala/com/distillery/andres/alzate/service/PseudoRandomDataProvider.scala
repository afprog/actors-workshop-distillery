package com.distillery.andres.alzate.service

import com.distillery.andres.alzate.exceptions.PseudoRandomStringGenerationException

import scala.util.Random

class PseudoRandomDataProvider extends DataProvider {

  val maxStringLength = 100
  val lengthToReturnLpfnString = 50
  val lengthToReturnException = 25

  override def getData(): String = {
    val length = util.Random.nextInt(maxStringLength)
    if (length % 30 == 0) {
      "Lpfn"
    }
    else {
      if (length % 50 == 0)
        throw PseudoRandomStringGenerationException(message = "PseudoRandom Generator Fail")
      else {
        val s = Random.alphanumeric.take(length).mkString
        s
      }
    }
  }
}
