package com.distillery.andres.alzate.exceptions

final case class PseudoRandomStringGenerationException(private val message: String = "",
                                                       private val cause: Throwable = None.orNull)
  extends Exception(message, cause)
