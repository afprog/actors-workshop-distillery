package com.distillery.andres.alzate.actors

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill, Props}
import com.distillery.andres.alzate.moldel.{ProcessCompleted, Report, Start, StartAllProcess, Stop, StopAllProcess}
import com.distillery.andres.alzate.service.PseudoRandomDataProvider
import com.distillery.andres.alzate.Main.system
import com.distillery.andres.alzate.moldel._

import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration


class Supervisor(workersCount: Int, workersTimeout: FiniteDuration) extends Actor with ActorLogging {

  val provider = new PseudoRandomDataProvider
  val workers: Seq[ActorRef] = createWorkers
  var reports: mutable.Buffer[Report] = scala.collection.mutable.Buffer.empty[Report]
  var workersFree: Int = 0

  override def receive: Receive = {
    case StartAllProcess =>
      log.info("Sending Start to workers")
      workers.foreach(_ ! Start)
    case StopAllProcess =>
      log.info("Stoping all workers")
      workers.foreach(_ ! Stop)
      log.info("Process Finished ")
      println(s"ElapsedTime - Bytes - WorkerStatus")
      reports.sortBy(_.elapsedTime)(Ordering[Option[Long]].reverse)
        .foreach(rep => println(s"${rep.elapsedTime} - ${rep.bytes} - ${rep.workerStatus}"))
      self ! PoisonPill
    case ProcessCompleted(report) => {
      log.info("---- Process completed worker {} status {}", sender().path.name, report.workerStatus)
      reports = reports += report
      workersFree = workersFree + 1
      if (workersFree == workersCount)
        self ! StopAllProcess
    }

  }

  override def postStop(): Unit = {
    super.postStop()
    log.info("Stopping actor system")
    context.system.terminate()
  }

  private def createWorkers: Seq[ActorRef] = {
    (1 to workersCount).map(x => system.actorOf(Worker.props(provider, workersTimeout, self), s"worker-$x"))
  }
}

object Supervisor {
  def props(workerCount: Integer, timeout: FiniteDuration) = Props.create(classOf[Supervisor], workerCount, timeout)

}
