package com.distillery.andres.alzate.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable, PoisonPill, Props}
import com.distillery.andres.alzate.exceptions.PseudoRandomStringGenerationException
import com.distillery.andres.alzate.moldel
import com.distillery.andres.alzate.moldel.{ProcessCompleted, Report, Start, Stop, WorkerStatus}
import com.distillery.andres.alzate.service.DataProvider
import com.distillery.andres.alzate.moldel.Report

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{DurationDouble, FiniteDuration}
import scala.language.postfixOps
import scala.util.Try

class Worker(provider: DataProvider, timeout: FiniteDuration, supervisor: ActorRef) extends Actor with ActorLogging {

  private val startTime: Long = System.currentTimeMillis()
  private var bytes: Long = 0L
  private var ticker: Option[Cancellable] = None
  private var timer: Option[Cancellable] = None

  override def receive: Receive = {
    case Start => start
    case Stop => stop
    case moldel.Process => process

  }

  private def process = {
    Try {
      //      log.info("process started")
      val data = provider.getData()
      bytes = bytes + data.getBytes().length
      //      log.info("String generated - {}" ,data)
      if (data.contains("Lpfn"))
        sendReport()
    }.recover {
      case ex: PseudoRandomStringGenerationException =>
        log.error(ex.getMessage)
        val report: Report = moldel.Report(WorkerStatus.FAILURE)
        supervisor ! ProcessCompleted(report)
        self ! PoisonPill
    }


  }

  private def sendReport(): Unit = {
    val report: Report = moldel.Report(WorkerStatus.SUCCESS, Some(getElapsedTime), Some(bytes))
    supervisor ! ProcessCompleted(report)
    self ! PoisonPill
  }

  private def getElapsedTime = System.currentTimeMillis() - startTime

  private def stop = {
    log.info("actor Stoped")
    val report: Report = moldel.Report(WorkerStatus.TIMEOUT)
    supervisor ! ProcessCompleted(report)
    self ! PoisonPill
  }

  private def start = {
    log.info("Worker started")
    timer = Option(context.system.scheduler.scheduleOnce(timeout, self, Stop))
    ticker = Option(context.system.scheduler.schedule(0 millisecond, 300 millisecond, self, moldel.Process))
  }

  override def postStop(): Unit = {
    ticker.foreach(_.cancel())
    ticker = None
    timer.foreach(_.cancel())
    timer = None
  }

}

object Worker {
  def props(provider: DataProvider, timeout: FiniteDuration, supervisor: ActorRef): Props = Props.create(classOf[Worker], provider, timeout, supervisor)
}
