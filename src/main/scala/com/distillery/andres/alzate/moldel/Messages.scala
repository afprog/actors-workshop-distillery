package com.distillery.andres.alzate.moldel

sealed trait WorkerMessage

case object Start extends WorkerMessage

case object Stop extends WorkerMessage

case object Process extends WorkerMessage

sealed trait SupervisorMessage

case object StartAllProcess extends SupervisorMessage

case object StopAllProcess extends SupervisorMessage

case class ProcessCompleted(report: Report) extends SupervisorMessage

case class ProcessTimeout(report: Report) extends SupervisorMessage

