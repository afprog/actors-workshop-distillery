package com.distillery.andres.alzate.moldel

import WorkerStatus.WorkerStatus

case class Report(workerStatus: WorkerStatus, elapsedTime: Option[Long] = None, bytes: Option[Long] = None)
