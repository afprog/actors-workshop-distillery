package com.distillery.andres.alzate.moldel

object WorkerStatus extends Enumeration {
  type WorkerStatus = Value
  val TIMEOUT, SUCCESS, FAILURE = Value
}