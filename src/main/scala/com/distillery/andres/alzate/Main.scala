package com.distillery.andres.alzate

import java.util.concurrent.TimeUnit
import akka.actor.ActorSystem
import com.distillery.andres.alzate.actors.Supervisor
import com.distillery.andres.alzate.moldel.StartAllProcess
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration.FiniteDuration
import scala.language.postfixOps

object Main extends App {
  implicit val system: ActorSystem = ActorSystem("system")
  val conf = ConfigFactory.load

  val workers = conf.getInt("workers")
  val timeout = FiniteDuration(conf.getInt("timeout"), TimeUnit.SECONDS)

  val supervisor = system.actorOf(Supervisor.props(workers, timeout), "supervisor")
  supervisor ! StartAllProcess
}
