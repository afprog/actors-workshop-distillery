name := "actors-workshop"

version := "0.1"

scalaVersion := "2.12.13"

lazy val akkaVersion = "2.6.14"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  "org.scalatest" %% "scalatest" % "3.0.5" % Test
)

assemblyJarName in assembly := "actors-workshop.jar"
target in assembly := new File("dist")
mainClass in assembly := Some("com.distillery.andres.alzate.Main")